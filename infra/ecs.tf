resource "aws_ecs_cluster" "main" {
  name = "emp-service-cluster"
}

//data "template_file" "emp_service_app" {
//  template = file("./templates/emp_service_app.json.tpl")
//
//  vars = {
//    app_image      = var.app_image
//    app_port       = var.app_port
//    fargate_cpu    = var.fargate_cpu
//    fargate_memory = var.fargate_memory
//    aws_region     = var.aws_region
//  }
//}

resource "aws_ecs_task_definition" "app" {
  family                   = "emp-service-app"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  container_definitions = <<EOF
    [
      {
        "containerDefinitions": [
          {
            "portMappings": [
              {
                "hostPort": 8080,
                "containerPort": 8080
              }
            ],
            "cpu": 1024,
            "memory": 2048,
            "memoryReservation": 7000,
            "image": "govindk408/employee_service:latest",
            "essential": true,
            "name": "emp_service_app"
          }
        ]
      }
    ]
    EOF
}

resource "aws_ecs_service" "main" {
  name            = "emp_service-app"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.app.arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks.id]
    subnets          = aws_subnet.private.*.id
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.app.id
    container_name   = "emp-service-app"
    container_port   = var.app_port
  }

  depends_on = [aws_alb_listener.front_end, aws_iam_role_policy_attachment.ecs_task_execution_role]
}