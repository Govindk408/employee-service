# Set up CloudWatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "emp_service_log_group" {
  name              = "/ecs/emp_service-app"
  retention_in_days = 30

  tags = {
    Name = "emp_service-log-group"
  }
}

resource "aws_cloudwatch_log_stream" "emp_service_log_stream" {
  name           = "emp_service-log-stream"
  log_group_name = aws_cloudwatch_log_group.emp_service_log_group.name
}